package com.example.homework7

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.homework7.databinding.ItemRecyclerviewLayoutBinding
import kotlinx.android.synthetic.main.item_recyclerview_layout.view.*

class RecyclerViewAdapter(private val items: MutableList<DataModel.Data>, private val activity: MainActivity): RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_recyclerview_layout, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onbind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var model: DataModel.Data
        fun onbind() {
            model =items[adapterPosition]
            val binding : ItemRecyclerviewLayoutBinding = DataBindingUtil.setContentView(activity, R.layout.item_recyclerview_layout)
            binding.itemModel = model
        }
    }
}