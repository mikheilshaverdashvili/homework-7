package com.example.homework7

import com.google.gson.annotations.SerializedName

class DataModel {
    var data:MutableList<Data> = ArrayList()

    class Data {
        @SerializedName("id")
        var id : Int = 0
        @SerializedName("descriptionEN")
        val descriptionEN : String = ""
        @SerializedName("titleEN")
        val titleEN : String = ""
        @SerializedName("cover")
        val cover : String = ""
    }
}